<?php

namespace MageMastery\Todo\Api;

use MageMastery\Todo\Api\Data\TaskInterface;

/**
 * @api
 */
interface CustomerTaskListInterface
{
    /**
     * @return MageMastery\Todo\Api\Data\TaskInterface[]
     */
    public function getList();
}
